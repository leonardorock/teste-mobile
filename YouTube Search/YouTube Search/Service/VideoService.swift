//
//  VideoService.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

class VideoService: Service {
    
    let endPoint: YouTubeAPI = .videos
    
    func fetchVideo(withIdentifier identifier: Identifier, resultHandler: ResultHandler<VideoListResponse>) {
        
        var parameters: [String : Any] = [:]
        
        parameters["id"] = identifier.videoId
        parameters["part"] = ["snippet", "statistics"].joined(separator: ",")
        parameters["key"] = apiKey
        
        request(parameters: parameters, resultHandler: resultHandler)
        
    }
    
}
