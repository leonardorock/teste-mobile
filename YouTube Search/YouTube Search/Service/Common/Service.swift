//
//  Service.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Alamofire
import CodableAlamofire

protocol Service {
    
    typealias ResultHandler<T> = (success: (T) -> Void, failure: (Error) -> Void, completion: () -> Void)
    
    var endPoint: YouTubeAPI { get }
    
}

extension Service {
    
    var apiKey: String {
        return "AIzaSyD6MEE0JBBcVFDLukPVjTElVwnB7EQ2HA0"
    }
    
    var decoder: JSONDecoder {
        let dateFormatter = DateFormatter()
        let decoder = JSONDecoder()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
    
    func request<T>(parameters: [String : Any]? = nil, resultHandler: ResultHandler<T>) where T : Decodable {
        Alamofire.request(endPoint, method: .get, parameters: parameters).responseDecodableObject(decoder: decoder) { (response: DataResponse<T>) in
            switch response.result {
            case .success(let value):
                resultHandler.success(value)
                break
            case .failure(let error):
                print(error)
                resultHandler.failure(error)
                break
            }
            resultHandler.completion()
        }
    }
    
}
