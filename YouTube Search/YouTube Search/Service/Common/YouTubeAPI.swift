//
//  YouTubeAPI.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/24/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Alamofire

enum YouTubeAPI: String, URLConvertible {
    case search, videos
    
    static let baseURL = "https://www.googleapis.com/youtube/v3/"
    
    func asURL() throws -> URL {
        return try YouTubeAPI.baseURL.appending(self.rawValue).asURL()
    }
}
