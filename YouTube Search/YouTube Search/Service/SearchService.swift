//
//  SearchService.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

class SearchService: Service {
    
    let endPoint: YouTubeAPI = .search
    
    func performSearch(withQuery query: String, nextPageToken: String? = nil, resultHandler: ResultHandler<SearchListResponse>) {
        
        var parameters: [String : Any] = [:]
        
        parameters["pageToken"] = nextPageToken
        parameters["q"] = query
        parameters["part"] = "snippet"
        parameters["maxResults"] = 25
        parameters["type"] = "video"
        parameters["key"] = apiKey
        
        request(parameters: parameters, resultHandler: resultHandler)
        
    }
    
}
