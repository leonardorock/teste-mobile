//
//  VideoSearchViewController.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit

class VideoSearchViewController: UIViewController {
    
    @IBOutlet weak var dismissSearchButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchAreaHeight: NSLayoutConstraint!
    @IBOutlet weak var searchAreaEqualStackHeight: NSLayoutConstraint!
    @IBOutlet weak var searchStackView: UIStackView!
    @IBOutlet weak var searchStackViewLeading: NSLayoutConstraint!
    @IBOutlet weak var searchStackViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var appLogoImageView: UIImageView!
    
    private var animationController = ContextualImageTransitionAnimationController()
    
    var presenter: VideoSearchPresenterDelegate!
    
    var searchQuery: String? {
        return searchTextField.text
    }
    
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = VideoSearchPresenter(searchService: SearchService())
        presenter.delegate = self
        registerForKeyboardNotification()
        setupTableView()
    }
    
    func setupTableView() {
        tableView.register(cellType: SearchResultTableViewCell.self)
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - Keyboard notifications
    
    func registerForKeyboardNotification() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo
        let value = userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue
        if let rect = value?.cgRectValue {
            presenter.willShowKeyboard(width: Float(rect.width), height: Float(rect.height))
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let searchQuery = searchQuery, searchQuery.isEmpty {
            presenter.dismissSearchButtonTapped()
        }
    }
    
    // MARK: - Interface builder actions
    
    @IBAction func dismissSearchButtonTapped(_ sender: Any) {
        presenter.dismissSearchButtonTapped()
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        presenter.searchButtonTapped()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSearchResultDetail" {
            let destination = segue.destination as! VideoDetailViewController
            destination.searchResult = sender as! SearchResultDataView
            destination.transitioningDelegate = self
        }
    }
    
}
// MARK: - Video search view delegate
extension VideoSearchViewController: VideoSearchViewDelegate {
    
    func changeViewState(to state: VideoSearchViewState) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.85, initialSpringVelocity: 0.8, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            switch state {
            case .initial:
                self.searchAreaHeight.priority = .defaultHigh
                self.searchAreaEqualStackHeight.priority = UILayoutPriority(rawValue: 999)
                self.searchStackView.axis = .vertical
                self.dismissSearchButton.alpha = 0.0
                self.dismissSearchButton.isEnabled = false
                self.searchButton.setTitle(NSLocalizedString("Search", comment: "Search placeholder"), for: .normal)
                self.searchStackViewLeading.constant = 16.0
                self.searchStackViewTrailing.constant = 16.0
                self.tableView.tableHeaderView = nil
                self.appLogoImageView.isHidden = false
                self.appLogoImageView.alpha = 1.0
                break
            case .typingWithoutResults(let keyboardHeight):
                self.searchAreaHeight.priority = UILayoutPriority(rawValue: 999)
                self.searchAreaEqualStackHeight.priority = .defaultLow
                self.searchAreaHeight.constant = self.mainStackView.frame.size.height - CGFloat(keyboardHeight)
                self.searchStackView.axis = .vertical
                self.dismissSearchButton.alpha = 0.0
                self.dismissSearchButton.isEnabled = false
                self.searchButton.setTitle(NSLocalizedString("Search", comment: "Search placeholder"), for: .normal)
                self.searchStackViewLeading.constant = 16.0
                self.searchStackViewTrailing.constant = 16.0
                self.tableView.tableHeaderView = nil
                self.appLogoImageView.isHidden = false
                self.appLogoImageView.alpha = 1.0
                break
            case .displayResults:
                self.searchAreaHeight.priority = UILayoutPriority(rawValue: 999)
                self.searchAreaEqualStackHeight.priority = .defaultLow
                self.searchAreaHeight.constant = 44.0
                self.searchStackView.axis = .horizontal
                self.dismissSearchButton.alpha = 1.0
                self.dismissSearchButton.isEnabled = true
                self.searchButton.setTitle(nil, for: .normal)
                self.searchStackViewLeading.constant = 8.0
                self.searchStackViewTrailing.constant = 8.0
                self.tableView.tableHeaderView = nil
                self.appLogoImageView.alpha = 0.0
                break
            }
        }) { [weak self] finished in
            self?.appLogoImageView.isHidden = self?.appLogoImageView.alpha == 0.0
        }
    }
    
    func invalidSearchQuery() {
        let redBorderAnimation = CABasicAnimation(keyPath: "borderColor")
        redBorderAnimation.duration = 2.0
        redBorderAnimation.fromValue = UIColor.red.cgColor
        redBorderAnimation.toValue = UIColor.red.cgColor
        
        
        let shakeAnimation = CABasicAnimation(keyPath: "position")
        shakeAnimation.duration = 0.1
        shakeAnimation.repeatCount = 2
        shakeAnimation.autoreverses = true
        shakeAnimation.fromValue = CGPoint(x: searchTextField.center.x - 10, y: searchTextField.center.y)
        shakeAnimation.toValue = CGPoint(x: searchTextField.center.x + 10, y: searchTextField.center.y)
        
        let animationGroup = CAAnimationGroup()
        animationGroup.animations = [shakeAnimation, redBorderAnimation]
        animationGroup.duration = 2.0
        
        searchTextField.layer.add(animationGroup, forKey: "invalidSearchQuery")
    }
    
    func setLoading(_ loading: Bool) {
        isLoading = loading
        if loading {
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityIndicator.startAnimating()
            tableView.tableFooterView = activityIndicator
        } else {
            tableView.tableFooterView = UIView()
        }
    }
    
    func showEmptyResultsView(forSearchQuery searchQuery: String) {
        let emptyResultsView = EmptyResultsView.loadFromNib()
        emptyResultsView.frame = CGRect(origin: .zero, size: tableView.frame.size)
        emptyResultsView.imageView.image = #imageLiteral(resourceName: "Youtube big")
        let emptyLabelTitle = String(format: NSLocalizedString("We didn't find any video with \"%@\"", comment: "empty search results for query title message"), searchQuery)
        emptyResultsView.titleLabel.text = emptyLabelTitle
        emptyResultsView.subtitleLabel.text = NSLocalizedString("You can try again with other term", comment: "try again message")
        tableView.tableHeaderView = emptyResultsView
    }
    
    func reloadResults() {
        tableView.reloadData()
    }
    
    func appendNewResults(itemsRange: CountableRange<Int>) {
        let indexPaths = itemsRange.map { IndexPath(row: $0, section: 0) }
        tableView.insertRows(at: indexPaths, with: .automatic)
    }
    
    func displayAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(.ok)
        present(alert, animated: true, completion: nil)
    }
    
    func dismissKeyboardIfNeeded() {
        searchTextField.resignFirstResponder()
    }
    
    func showResultDetails(for searchResult: SearchResultDataView) {
        performSegue(withIdentifier: "showSearchResultDetail", sender: searchResult)
    }
    
}
// MARK: - TextField delegate
extension VideoSearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            presenter.searchButtonTapped()
        }
        return true
    }
}
// MARK: - Scroll view delegate
extension VideoSearchViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let reachedTheBottom = scrollView.contentOffset.y + scrollView.frame.size.height > scrollView.contentSize.height
        if reachedTheBottom && !isLoading {
            presenter.fetchNextPage()
        }
    }
}
// MARK: - Table view data source
extension VideoSearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfResults()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SearchResultTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.setup(with: presenter.searchResult(at: indexPath.row))
        return cell
    }
    
}
// MARK: - Table view delegate
extension VideoSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectResult(at: indexPath.row)
    }
    
}
// MARK: - View controller transition Delegate
extension VideoSearchViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let videoDetailViewController = presented as? VideoDetailViewController
        animationController.setupTransition(image: selectedImageView?.image, from: self, to: videoDetailViewController)
        return animationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let videoDetailViewController = dismissed as? VideoDetailViewController
        animationController.setupTransition(image: selectedImageView?.image, from: videoDetailViewController, to: self)
        return animationController
    }
}
// MARK: - View controller transition Delegate
extension VideoSearchViewController: ContextualImageTransitionProtocol {
    
    var selectedImageView: UIImageView? {
        guard let indexPath = tableView.indexPathForSelectedRow, let cell = tableView.cellForRow(at: indexPath) as? SearchResultTableViewCell else {
            return nil
        }
        return cell.thumbnailImageView
    }
    
    var imageViewFrame: CGRect? {
        guard let frame = selectedImageView?.frame else {
            return nil
        }
        return selectedImageView?.convert(frame, to: view)
    }
    
    func transitionSetup() {
        selectedImageView?.alpha = 0
    }
    
    func transitionCleanUp() {
        selectedImageView?.alpha = 1
    }
    
}


