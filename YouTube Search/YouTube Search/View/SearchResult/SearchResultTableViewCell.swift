//
//  SearchResultTableViewCell.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/24/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit
import Reusable

final class SearchResultTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var videoDescriptionLabel: UILabel!
    
    func setup(with searchResult: SearchResultDataView) {
        thumbnailImageView.setImage(withURL: searchResult.thumbnailURL, placeholderImage: #imageLiteral(resourceName: "Thumbnail Placeholder"))
        videoTitleLabel.text = searchResult.title
        videoDescriptionLabel.text = searchResult.description
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImageView.af_cancelImageRequest()
    }
    
}
