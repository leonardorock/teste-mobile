//
//  VideoDetailViewController.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/26/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit
import WebKit

class VideoDetailViewController: UIViewController {
    
    @IBOutlet weak var videoWebView: WKWebView!
    @IBOutlet weak var thumbnailImageView: UIImageView?
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var channelTitleLabel: UILabel!
    @IBOutlet weak var videoDescriptionTextView: UITextView!
    @IBOutlet weak var publishedAtLabel: UILabel!
    @IBOutlet weak var videoViewsLabel: UILabel!
    @IBOutlet weak var videoLikesLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var presenter: VideoDetailPresenterDelegate!
    var searchResult: SearchResultDataView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = VideoDetailPresenter(videoService: VideoService())
        presenter.delegate = self
        presenter.viewDidLoad(with: searchResult)
    }
    
    @IBAction func dismissVideo(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
// MARK: - Video detail view delegate
extension VideoDetailViewController: VideoDetailViewDelegate {
    
    func updateView(with searchResult: SearchResultDataView) {
        if let urlRequest = searchResult.videoEmbedURLRequest {
            videoWebView.navigationDelegate = self
            videoWebView.load(urlRequest)
        }
        thumbnailImageView?.setImage(withURL: searchResult.thumbnailURL, placeholderImage: nil)
        videoTitleLabel.text = searchResult.title
        channelTitleLabel.text = searchResult.channelTitle
        videoDescriptionTextView.text = searchResult.description
        publishedAtLabel.text = searchResult.publishedAt
        videoViewsLabel.text = nil
        videoLikesLabel.text = nil
    }
    
    func updateVideo(with videoResult: VideoResultDataView) {
        videoDescriptionTextView.text = videoResult.description
        videoViewsLabel.text = videoResult.viewCount
        videoLikesLabel.text = videoResult.likeCount
    }
    
    func displayAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(.ok)
        present(alert, animated: true, completion: nil)
    }
    
}
// MARK: - Web view navigation delegate
extension VideoDetailViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicatorView.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        thumbnailImageView?.removeFromSuperview()
        activityIndicatorView.stopAnimating()
    }
}
// MARK: - Bar positioning delegate
extension VideoDetailViewController: UIBarPositioningDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}
// MARK: - Contextual Image Transition Protocol
extension VideoDetailViewController: ContextualImageTransitionProtocol {
    var imageViewFrame: CGRect? {
        return videoWebView.convert(videoWebView.frame, to: view)
    }
    
    func transitionSetup() {
        thumbnailImageView?.alpha = 0.0
    }
    
    func transitionCleanUp() {
        thumbnailImageView?.alpha = 1.0
    }
    
}

