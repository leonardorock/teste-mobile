//
//  ThumbnailDetail.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct ThumbnailDetail: Codable {
    let url: URL
    let width: Int
    let height: Int
}
