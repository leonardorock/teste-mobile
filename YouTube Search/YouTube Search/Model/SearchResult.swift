//
//  SearchResult.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

struct SearchResult: Codable {
    let id: Identifier
    let snippet: Snippet
}
