//
//  Statistics.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

struct Statistics: Codable {
    let viewCount: String
    let likeCount: String
}
