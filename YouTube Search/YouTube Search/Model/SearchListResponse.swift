//
//  SearchListResponse.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

struct SearchListResponse: Codable {
    let nextPageToken: String?
    let items: [SearchResult]
}
