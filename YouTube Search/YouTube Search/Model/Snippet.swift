//
//  Snippet.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct Snippet: Codable {
    let publishedAt: Date
    let channelId: String
    let title: String
    let description: String?
    let thumbnails: Thumbnails
    let channelTitle: String
}
