//
//  Thumbnails.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/23/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

struct Thumbnails: Codable {
    let low: ThumbnailDetail?
    let medium: ThumbnailDetail?
    let high: ThumbnailDetail?
    let standard: ThumbnailDetail?
    let maxres: ThumbnailDetail?
    
    enum CodingKeys: String, CodingKey {
        case low = "default", medium, high, standard, maxres
    }
}
