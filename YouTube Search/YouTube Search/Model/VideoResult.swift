//
//  VideoResult.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/26/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

struct VideoResult: Codable {
    let id: String
    let snippet: Snippet
    let statistics: Statistics?
}
