//
//  EmptyResultsView.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/25/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit
import Reusable

class EmptyResultsView: UIView, NibLoadable {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

}
