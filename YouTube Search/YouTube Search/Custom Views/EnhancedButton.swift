//
//  EnhancedButton.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/25/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit

@IBDesignable
class EnhancedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
}
