//
//  VideoDetailProtocols.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/26/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

protocol VideoDetailViewDelegate: class {
    
    func updateView(with searchResult: SearchResultDataView)
    func updateVideo(with videoResult: VideoResultDataView)
    func displayAlert(title: String?, message: String?)
    
}

protocol VideoDetailPresenterDelegate {
    
    weak var delegate: VideoDetailViewDelegate? { get set }
    
    func viewDidLoad(with searchResult: SearchResultDataView)
    
}
