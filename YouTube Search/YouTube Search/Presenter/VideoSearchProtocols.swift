//
//  VideoSearchProtocols.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/25/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

enum VideoSearchViewState {
    case initial,
    typingWithoutResults(keyboardHeight: Float),
    displayResults
}

protocol VideoSearchViewDelegate: class {
    
    var searchQuery: String? { get }
    
    func changeViewState(to state: VideoSearchViewState)
    func invalidSearchQuery()
    func setLoading(_ loading: Bool)
    func showEmptyResultsView(forSearchQuery searchQuery: String)
    func reloadResults()
    func appendNewResults(itemsRange: CountableRange<Int>)
    func displayAlert(title: String?, message: String?)
    func dismissKeyboardIfNeeded()
    func showResultDetails(for searchResult: SearchResultDataView)
    
}

protocol VideoSearchPresenterDelegate: class {
    
    weak var delegate: VideoSearchViewDelegate? { get set }
    
    init(searchService: SearchService)
    
    func willShowKeyboard(width: Float, height: Float)
    func searchButtonTapped()
    func fetchNextPage()
    func dismissSearchButtonTapped()
    func numberOfResults() -> Int
    func searchResult(at index: Int) -> SearchResultDataView
    func didSelectResult(at index: Int)
    
}
