//
//  VideoSearchPresenter.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/24/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

class VideoSearchPresenter {
    
    weak var delegate: VideoSearchViewDelegate?
    
    let searchService: SearchService
    
    var searchResults: [SearchResult] = []
    var nextPageToken: String? = nil
    
    
    required init(searchService: SearchService) {
        self.searchService = searchService
    }
    
    func resetResults() {
        self.nextPageToken = nil
        self.searchResults = []
        delegate?.reloadResults()
    }
    
}

extension VideoSearchPresenter: VideoSearchPresenterDelegate {
    
    func willShowKeyboard(width: Float, height: Float) {
        delegate?.changeViewState(to: .typingWithoutResults(keyboardHeight: height))
    }
    
    func searchButtonTapped() {
        guard let searchQuery = delegate?.searchQuery, !searchQuery.isEmpty else {
            delegate?.invalidSearchQuery()
            return
        }
        resetResults()
        delegate?.dismissKeyboardIfNeeded()
        delegate?.changeViewState(to: .displayResults)
        
        fetchNextPage()
    }
    
    func fetchNextPage() {
        guard let searchQuery = delegate?.searchQuery, !searchQuery.isEmpty else {
            return
        }
        self.delegate?.setLoading(true)
        searchService.performSearch(withQuery: searchQuery, nextPageToken: nextPageToken, resultHandler: (success: { [weak self] searchListResponse in
            guard let previousCount = self?.searchResults.count else {
                return
            }
            let newCount = previousCount + searchListResponse.items.count
            let newItemsRange = previousCount..<newCount
            self?.searchResults.append(contentsOf: searchListResponse.items)
            self?.nextPageToken = searchListResponse.nextPageToken
            if let searchResults = self?.searchResults, searchResults.isEmpty {
                self?.delegate?.showEmptyResultsView(forSearchQuery: searchQuery)
            } else {
                self?.delegate?.appendNewResults(itemsRange: newItemsRange)
            }
        }, failure: { [weak self] error in
            let title = "Error"
            let message = error.localizedDescription
            self?.delegate?.displayAlert(title: title, message: message)
        }, completion: { [weak self] in
            self?.delegate?.setLoading(false)
        }))
    }
    
    func dismissSearchButtonTapped() {
        resetResults()
        delegate?.dismissKeyboardIfNeeded()
        delegate?.changeViewState(to: .initial)
    }
    
    func numberOfResults() -> Int {
        return searchResults.count
    }
    
    func searchResult(at index: Int) -> SearchResultDataView {
        return SearchResultDataView(model: searchResults[index])
    }
    
    func didSelectResult(at index: Int) {
        let searchResult = searchResults[index]
        delegate?.showResultDetails(for: SearchResultDataView(model: searchResult))
    }
    
}
