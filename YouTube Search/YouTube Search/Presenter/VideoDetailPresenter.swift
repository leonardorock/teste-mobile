//
//  VideoDetailPresenter.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/26/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

class VideoDetailPresenter: VideoDetailPresenterDelegate {
    
    weak var delegate: VideoDetailViewDelegate?
    
    var searchResult: SearchResult? = nil
    var videoService: VideoService!
    
    init(videoService: VideoService) {
        self.videoService = videoService
    }
    
    func viewDidLoad(with searchResult: SearchResultDataView) {
        self.searchResult = searchResult.model
        delegate?.updateView(with: searchResult)
        updateVideoData()
    }
    
    func updateVideoData() {
        guard let identifier = searchResult?.id else {
            return
        }
        videoService.fetchVideo(withIdentifier: identifier, resultHandler: (success: { [weak self] videoListResponse in
            guard let item = videoListResponse.items.first else {
                return
            }
            self?.delegate?.updateVideo(with: VideoResultDataView(model: item))
        }, failure: { [weak self] error in
            let title = "Error"
            let message = error.localizedDescription
            self?.delegate?.displayAlert(title: title, message: message)
        }, completion: {}))
    }
    
}
