//
//  VideoResultDataView.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/26/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct VideoResultDataView {
    var model: VideoResult
    
    var description: String? {
        return model.snippet.description
    }
    
    var likeCount: String? {
        guard let likeCount = model.statistics?.likeCount else {
            return nil
        }
        let likesFormat = NSLocalizedString("%@ likes", comment: "likes count")
        return String(format: likesFormat, likeCount)
    }
    
    var viewCount: String? {
        guard let viewCount = model.statistics?.viewCount else {
            return nil
        }
        let viewsFormat = NSLocalizedString("%@ views", comment: "views count")
        return String(format: viewsFormat, viewCount)
    }
}
