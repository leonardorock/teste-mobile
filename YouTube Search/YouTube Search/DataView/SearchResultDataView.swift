//
//  SearchResultDataView.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/24/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct SearchResultDataView {
    
    let model: SearchResult
    
    static var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }
    
    var thumbnailURL: URL? {
        return model.snippet.thumbnails.medium?.url
    }
    
    var title: String? {
        return model.snippet.title
    }
    
    var channelTitle: String? {
        return model.snippet.channelTitle
    }
    
    var description: String? {
        return model.snippet.description
    }
    
    var publishedAt: String? {
        let date = model.snippet.publishedAt
        return SearchResultDataView.dateFormatter.string(from: date)
    }
    
    var videoEmbedURLRequest: URLRequest? {
        let id = model.id.videoId
        guard let url = URL(string: "https://www.youtube.com/embed/\(id)") else {
            return nil
        }
        return URLRequest(url: url)
    }
    
}
