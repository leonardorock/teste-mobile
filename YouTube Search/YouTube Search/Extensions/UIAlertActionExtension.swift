//
//  UIAlertActionExtension.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/24/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit

extension UIAlertAction {
    
    static let ok = UIAlertAction(title: NSLocalizedString("OK", comment: "ok alert button title"), style: .cancel, handler: nil)
    
}
