//
//  UIImageViewExtension.swift
//  YouTube Search
//
//  Created by Leonardo Oliveira on 10/24/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImageView {
    
    func setImage(withURL url: URL?, placeholderImage: UIImage?) {
        guard let url = url else {
            image = placeholderImage
            return
        }
        af_setImage(withURL: url, placeholderImage: placeholderImage)
    }
    
}
