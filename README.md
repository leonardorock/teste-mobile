# Teste Madeinweb: Mobile

## Table of contents
- Goal
- About
- Localization
- screenshots
- Architecture
- Frameworks
- Running the application

## Goal
The goal for this test is to build an application that allows the user to search and watch youtube videos with an amazing user experience

## About
The app is called “YouTube Search” (Super creative).
With it, I wanted to deliver a true native iOS experience without compromising the visual identity of the application, giving users an app with a nice look and feel.

## Localization
The app is localized for the following languages:
- English
- Portuguese

## Screenshots
![Initial Screen](/Screenshots/initial.png)
![Typing Screen](/Screenshots/typing.png)
![Search Results Screen](/Screenshots/search_results.png)
![Video Detail Screen](/Screenshots/video_detail.png)
![Empty Result Screen](/Screenshots/empty_result.png)

## Architecture
This project was built using the MVP (Model View Presenter) architecture

## Frameworks
To build this project I’ve used some amazing third-party frameworks which are:

- [Alamofire](https://github.com/Alamofire/Alamofire): *Network framework*
- [AlamofireCodable](https://github.com/Otbivnoe/CodableAlamofire): *JSON Helper to Parse Alamofire responses to values types or objects using Swift’s Codable Protocol*
- [AlamofireImage](https://github.com/Alamofire/AlamofireImage): *Image caching framwork*
- [Reusable](https://github.com/AliSoftware/Reusable): *A very handful framework for reusable views, even if they are nib loaded*

## Running the application
To run this application, you’ll need to:

- Clone or download this project
- Open your Mac terminal and run the following command to install cocoa pods (If already you have cocoa pods installed you can skip this step)

```
$ sudo gem install cocoapods
```

- Go to the project directory using terminal and run the following command:

```
$ pod install
```

- Open the project workspace with on Xcode and run the application 😊

That’s it


